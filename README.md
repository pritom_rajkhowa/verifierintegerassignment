# VIAP-SVCOMP2018

## This page is the submission of VIAP -- a software verification tool to the 2018 Competition on Software Verification SVCOMP.

## Team
```
Pritom Rajkhowa (HKUST)
```
```
Fangzhen Lin (HKUST)
```

## Download

[viap.zip](https://bitbucket.org/pritom_rajkhowa/verifierintegerassignment/raw/6fbe7baf9be7630eaea05737f121017146361bc3/viap.zip)


## Run
```
python PATH_TO_VIAP/viap_tool.py [OPTIONS] file
```
  
```
--spec=SPEC SPEC is the property file.
```

## Categories
## VIAP is participating in the following sub-categories.
```
-ReachSafety-Arrays, 
```
```
-ReachSafety-Loops,
```
```
-ReachSafety-Recurrences .
```
## Results
## Output contains the string:
```
-Result TRUE when the program is safe
```
```
-Result FALSE when a counterexample is found.
```
```
-Result UNKNOWN otherwise.
```
# VIAP-SVCOMP2018
